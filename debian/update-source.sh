#!/bin/bash

if [ ${#@} -eq 0 ]; then
    echo "Usage: $0 UPSTREAM_TAG" >&2
    exit 0
fi

upstream_tag="$1"
version=$(echo $upstream_tag| sed 's,^v,,')
archive_name="aws-crt-python-${version}.tar"
prefix="aws-crt-python-${version}/"
git checkout "${upstream_tag}"
git submodule update --init
git archive --format tar -o "$archive_name" --prefix="$prefix" "$upstream_tag"
# descend recursively and archive each submodule
PREFIX="$prefix" git submodule \
      --quiet foreach \
      --recursive 'git archive --format tar --prefix="${PREFIX}${displaypath}/" -o submodule.tar HEAD'
# concatenate with main archive
TOPDIR=$(pwd) ARCHIVE_NAME="$archive_name" git submodule \
      --quiet foreach \
      --recursive \
      'cd $TOPDIR; tar --concatenate --file="$ARCHIVE_NAME" $displaypath/submodule.tar; rm -fv $displaypath/submodule.tar'
gzip -9 "$archive_name"
git submodule deinit --all
git checkout debian/sid


